FROM eclipse-temurin:22-jdk-alpine
LABEL authors="Benjamin Dittwald"

RUN apk update && \
    apk upgrade && \
    apk add docker maven